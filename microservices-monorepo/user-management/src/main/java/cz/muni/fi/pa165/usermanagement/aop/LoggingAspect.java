package cz.muni.fi.pa165.usermanagement.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class LoggingAspect {

    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Pointcut("within(@org.springframework.stereotype.Repository *)" +
        " || within(@org.springframework.stereotype.Service *)" +
        " || within(@org.springframework.web.bind.annotation.RestController *)")
    public void applicationPackagePointcut() {
        // Method is empty as this is just a Pointcut, the implementations are in the advices.
    }

    // Advice that logs methods when they are entered
    @Before("applicationPackagePointcut()")
    public void logBefore(JoinPoint joinPoint) {
        Logger log = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        String args = Arrays.stream(joinPoint.getArgs())
            .map(arg -> arg != null ? arg.toString() : "null")
            .collect(Collectors.joining(", "));
        String timestamp = LocalDateTime.now().format(dateTimeFormatter);
        log.info("Enter: {}#{} with arguments [{}] at {}", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), args, timestamp);
    }

    // Advice that logs methods when they exit
    @AfterReturning(pointcut = "applicationPackagePointcut()", returning = "result")
    public void logAfterReturning(JoinPoint joinPoint, Object result) {
        Logger log = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        String resultDescription = result != null ? result.toString() : "null";
        String timestamp = LocalDateTime.now().format(dateTimeFormatter);
        log.info("Exit: {}#{} returned [{}] at {}", joinPoint.getSignature().getDeclaringTypeName(),
            joinPoint.getSignature().getName(), resultDescription, timestamp);
    }
}
