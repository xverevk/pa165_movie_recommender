package cz.muni.fi.pa165.moviesmanagement.mapper;

import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MovieMapperTest {

    private MovieMapper movieMapper;

    @BeforeEach
    public void setUp() {
        movieMapper = new MovieMapper();
    }

    @Test
    public void convertToMovieDtoFromEntity_allCorrect_returnsMovieDto() {
        // Arrange
        Movie movie = new Movie();
        movie.setId(1L);
        movie.setName("Test Movie");
        movie.setDirector("Test Director");

        // Act
        MovieDto movieDto = movieMapper.convertToDto(movie);

        // Assert
        assertNotNull(movieDto);
        assertEquals(movie.getId(), movieDto.getId());
        assertEquals(movie.getName(), movieDto.getName());
        assertEquals(movie.getDirector(), movieDto.getDirector());
    }

    @Test
    public void convertToEntityFromMovieDto_allCorrect_returnsMovie() {
        // Arrange
        MovieDto movieDto = new MovieDto();
        movieDto.setId(1L);
        movieDto.setName("Test Movie");
        movieDto.setDirector("Test Director");

        // Act
        Movie movie = movieMapper.convertToEntity(movieDto);

        // Assert
        assertNotNull(movie);
        assertEquals(movieDto.getId(), movie.getId());
        assertEquals(movieDto.getName(), movie.getName());
        assertEquals(movieDto.getDirector(), movie.getDirector());
    }
}