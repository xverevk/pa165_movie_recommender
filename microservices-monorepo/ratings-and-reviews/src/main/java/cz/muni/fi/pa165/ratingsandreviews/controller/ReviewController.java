package cz.muni.fi.pa165.ratingsandreviews.controller;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.facade.ReviewFacade;
import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.specification.ReviewSpecification;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

/**
 * Controller for handling review-related operations.
 */
@OpenAPIDefinition(
        info = @Info(title = "Movie Recommender - Recommendations",
                version = "0.1.0",
                description = "Recommendations microservice for the Movie Recommender Web application",
                contact = @Contact(name = "Michal Reznik", email = "493229@mail.muni.cz"),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html"
                )
        ),
        servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
                @ServerVariable(
                        name = "scheme",
                        allowableValues = {"http", "https"}, defaultValue = "http"
                ),
                @ServerVariable(name = "server", defaultValue = "localhost"),
                @ServerVariable(name = "port", defaultValue = "8082"),
        })
)
@RestController
@RequestMapping(path = "/api/reviews", produces = MediaType.APPLICATION_JSON_VALUE)
public class ReviewController {
    /**
     * The ReviewFacade instance.
     */
    private final ReviewFacade reviewFacade;

    /**
     * Constructs a ReviewController with the given ReviewFacade.
     *
     * @param facade The ReviewFacade to be used.
     */
    public ReviewController(final ReviewFacade facade) {
        this.reviewFacade = facade;
    }

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review DTO.
     */
    @PostMapping()
    @Operation(
            summary = "Add new review",
            description = "Create a new review by its DTO"
    )
    public ResponseEntity<ReviewDTO> addReview(@RequestBody final ReviewDTO newReview) {
        ReviewDTO addedReview = reviewFacade.addReview(newReview);
        return ResponseEntity.status(HttpStatus.CREATED).body(addedReview);
    }

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review DTO.
     */
    @DeleteMapping("/{reviewId}")
    @Operation(
            summary = "Delete review",
            description = "Delete the review with given id"
    )
    public ResponseEntity<Long> deleteReview(@PathVariable final Long reviewId) {
        if (reviewFacade.deleteReview(reviewId)) {
            return ResponseEntity.ok(reviewId);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review DTO.
     */
    @PutMapping()
    @Operation(
            summary = "Update review",
            description = "Update the review with given id"
    )
    public ResponseEntity<ReviewDTO> updateReview(@RequestBody final ReviewDTO updatedReview) {
        ReviewDTO updated = reviewFacade.updateReview(updatedReview);
        return ResponseEntity.ok(updated);
    }

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review DTO.
     */
    @GetMapping("/{reviewId}")
    @Operation(
            summary = "Get review",
            description = "Get the review with given id"
    )
    public ResponseEntity<ReviewDTO> getReview(@PathVariable final Long reviewId) {
        Optional<ReviewDTO> review = reviewFacade.getReview(reviewId);
        return review.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    /**
     * Retrieves reviews by movie ID.
     *
     * @param movieId The ID of the movie whose reviews are to be retrieved.
     * @return List of review DTOs.
     */
    @GetMapping("/movie/{movieId}")
    @Operation(summary = "Get movie reviews", description = "Get reviews for movie id"
    )
    public ResponseEntity<Page<ReviewDTO>> getReviewsByMovieId(@PathVariable final Long movieId) {
        List<ReviewDTO> reviews = reviewFacade.getReviewsByUserId(movieId);
        return ResponseEntity.ok(new PageImpl<>(reviews));
    }

    /**
     * Retrieves reviews by user ID.
     *
     * @param userId The ID of the user whose reviews are to be retrieved.
     * @return List of review DTOs.
     */
    @GetMapping("/user/{userId}")
    @Operation(summary = "Get users reviews", description = "Get reviews for users id"
    )
    public ResponseEntity<Page<ReviewDTO>> getReviewsByUserId(@PathVariable Long userId) {
        List<ReviewDTO> reviews = reviewFacade.getReviewsByUserId(userId);
        return ResponseEntity.ok(new PageImpl<>(reviews));
    }

    /**
     * Retrieves a paginated list of all reviews.
     * This endpoint allows you to retrieve all reviews from the database, optionally ordered
     * by overall rating or by the rating for a specific film property.
     *
     * @param orderByOverallRating (optional) boolean flag indicating whether to order results by overall rating
     *                              (true for ascending, false for descending)
     * @param orderByProperty (optional) the FilmProperty to order results by (e.g., ACTING, DIRECTING)
     * @param ascending (optional) boolean flag indicating the sort direction for the chosen order by property
     *                              (true for ascending, false for descending, defaults to true)
     * @return a ResponseEntity containing a paginated list of ReviewDTO objects representing the retrieved reviews
     */
    @GetMapping()
    @Operation(summary = "Get all reviews", description = "Get reviews for users id"
    )
    public ResponseEntity<Page<ReviewDTO>> getAllReviews(
            @RequestParam(value = "orderByOverallRating", required = false) boolean orderByOverallRating,
            @RequestParam(value = "orderByProperty", required = false) FilmProperty orderByProperty,
            @RequestParam(value = "ascending", defaultValue = "true") boolean ascending) {

        Specification<Review> spec = Specification.where(null);

        if (orderByOverallRating) {
            spec = spec.and(ReviewSpecification.orderByOverallRating(ascending));
        }

        if (orderByProperty != null) {
            spec = spec.and(ReviewSpecification.orderByRatingForProperty(orderByProperty, ascending));
        }

        List<ReviewDTO> reviews = reviewFacade.getReviewsBySpecification(spec);

        return ResponseEntity.ok(new PageImpl<>(reviews));
    }
}
