package cz.muni.fi.pa165.usermanagement.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;

@Schema(title = "User Update DTO",
    description = "Represents the data transfer object for user update operations")
public record UserUpdateDTO(
    @NotBlank
    @Schema(description = "username", example = "user123456")
    @JsonProperty("userName")
    String userName,

    @NotBlank
    @Schema(description = "email", example = "user123456@gmail.com")
    @JsonProperty("email")
    String email,

    @NotBlank
    @Schema(description = "encrypted password", example = "$#!bcvxzbvxc0-=532",
        requiredMode = Schema.RequiredMode.NOT_REQUIRED)
    @JsonProperty("encryptedPassword")
    String encryptedPassword,

    @NotBlank
    @Schema(description = "user type", example = "USER")
    @JsonProperty("userType")
    UserType userType
) {
}
