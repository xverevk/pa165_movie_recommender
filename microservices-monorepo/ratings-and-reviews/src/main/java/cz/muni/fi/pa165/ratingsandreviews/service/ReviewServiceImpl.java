package cz.muni.fi.pa165.ratingsandreviews.service;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import cz.muni.fi.pa165.ratingsandreviews.repository.ReviewRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Implementation of ReviewService interface.
 */
@Service
public class ReviewServiceImpl implements ReviewService {

    /**
     * The review repository.
     */
    private final ReviewRepository reviewRepository;

    /**
     * Constructor for ReviewServiceImpl.
     *
     * @param repository The review repository.
     */
    public ReviewServiceImpl(final ReviewRepository repository) {
        this.reviewRepository = repository;
    }

    /**
     * Get all reviews by user ID
     *
     * @param userId users id.
     */
    @Override
    public List<Review> getAllReviewsByUserId(Long userId) {
        return reviewRepository.findByUserId(userId);
    }
    /**
     * Get all reviews by movie ID
     *
     * @param movieId movies id.
     */
    @Override
    public List<Review> getAllReviewsByMovieId(Long movieId) {
        return reviewRepository.findByMovieId(movieId);
    }

    /**
     * Get all reviews
     *
     * @param spec specification for query.
     */
    @Override
    public List<Review> getAllReviewsSortedBySpecification(Specification<Review> spec) {
        return reviewRepository.findAll(spec);
    }

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review.
     */
    @Override
    public Review addReview(final Review newReview) {
        reviewRepository.save(newReview);
        return newReview;
    }

    /**
     * Deletes a review by ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review.
     * @throws EntityNotFoundException if the review with the given ID does not exist.
     */
    @Override
    public Boolean deleteReview(final Long reviewId) {
        Optional<Review> review = reviewRepository.findById(reviewId);
        if (review.isPresent()) {
            reviewRepository.delete(review.get());
            return true;
        } else {
            throw new EntityNotFoundException("Review with id " + reviewId + "does not exist");
        }
    }

    /**
     * Retrieves a review by ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review.
     * @throws EntityNotFoundException if the review with the given ID does not exist.
     */
    @Override
    public Optional<Review> getReview(final Long reviewId) {
        return reviewRepository.findById(reviewId);
    }

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review.
     */
    @Override
    @Transactional
    public Review updateReview(final Review updatedReview) {
        return reviewRepository.save(updatedReview);
    }
}
