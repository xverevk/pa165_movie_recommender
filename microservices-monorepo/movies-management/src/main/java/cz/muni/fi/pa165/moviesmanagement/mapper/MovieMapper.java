package cz.muni.fi.pa165.moviesmanagement.mapper;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import org.springframework.stereotype.Component;

@Component
public class MovieMapper {

    /**
     * Converts a movie entity to a movie DTO.
     *
     * @param movie The movie entity to be converted.
     * @return The movie DTO.
     */
    public MovieDto convertToDto(final Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setName(movie.getName());
        movieDto.setDirector(movie.getDirector());
        movieDto.setDescription(movie.getDescription());
        movieDto.setActors(movie.getActors());
        movieDto.setGenres(movie.getGenres());
        movieDto.setImageUrl(movie.getImageUrl());
        return movieDto;
    }

    /**
     * Converts a movie DTO to a movie entity.
     *
     * @param movieDto The movie DTO to be converted.
     * @return The movie entity.
     */
    public Movie convertToEntity(final CreateMovieDto movieDto) {
        Movie movie = new Movie();
        movie.setName(movieDto.name());
        movie.setDirector(movieDto.director());
        movie.setDescription(movieDto.description());
        movie.setActors(movieDto.actors());
        movie.setGenres(movieDto.genres());
        movie.setImageUrl(movieDto.imageUrl());
        return movie;
    }

    /**
     * Converts a movie DTO to a movie entity.
     *
     * @param movieDto The movie DTO to be converted.
     * @return The movie entity.
     */
    public Movie convertToEntity(final MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setId(movieDto.getId());
        movie.setName(movieDto.getName());
        movie.setDirector(movieDto.getDirector());
        movie.setDescription(movieDto.getDescription());
        movie.setActors(movieDto.getActors());
        movie.setGenres(movieDto.getGenres());
        movie.setImageUrl(movieDto.getImageUrl());
        return movie;
    }
}
