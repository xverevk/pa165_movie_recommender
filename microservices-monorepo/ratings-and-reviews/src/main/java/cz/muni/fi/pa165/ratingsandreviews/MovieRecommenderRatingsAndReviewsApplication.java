package cz.muni.fi.pa165.ratingsandreviews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
@SpringBootApplication
public class MovieRecommenderRatingsAndReviewsApplication {

    /**
	 * Main method to run the application.
	 * @param args command line arguments
	 */
	public static void main(final String[] args) {
		SpringApplication.run(MovieRecommenderRatingsAndReviewsApplication.class, args);
	}

}
