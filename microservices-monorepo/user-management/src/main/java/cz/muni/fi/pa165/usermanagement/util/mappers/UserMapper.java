package cz.muni.fi.pa165.usermanagement.util.mappers;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

/**
 * This interface defines mapping methods between User entities and their Data Transfer Object (DTO) counterparts.
 * It uses Spring's mapping framework to automate the conversion process.
 */
@Mapper(componentModel = "spring")
public interface UserMapper {

    /**
     * Converts a User entity to a UserDTO.
     *
     * @param user The User entity to convert.
     * @return The converted UserDTO.
     */
    UserDTO userToUserDTO(User user);

    /**
     * Converts a list of User entities to a list of UserDTOs.
     *
     * @param users The list of User entities to convert.
     * @return The list of converted UserDTOs.
     */
    List<UserDTO> usersToUserDTOs(List<User> users);

    /**
     * Converts a UserRegistrationDTO to a User entity.
     * This method ignores the 'id' property during the conversion process.
     *
     * @param userDto The UserRegistrationDTO to convert.
     * @return The converted User entity.
     */
    @Mapping(target = "id", ignore = true)
    User userRegistrationDTOToUser(UserRegistrationDTO userDto);

    /**
     * Updates an existing User entity with values from a UserUpdateDTO.
     * This method ignores the 'id' property during the update process.
     *
     * @param dto    The UserUpdateDTO containing updated values.
     * @param entity The User entity to be updated.
     */
    @Mapping(target = "id", ignore = true)
    void updateEntityWithDto(UserUpdateDTO dto, @MappingTarget User entity);

}
