package cz.muni.fi.pa165.ratingsandreviews.service;

import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service interface for managing reviews.
 */
@Service
@Primary
public interface ReviewService {

    /**
     * Get all reviews by specification
     *
     * @param spec specification of sort
     * @return List of reviews.
     */
    @Transactional
    List<Review> getAllReviewsSortedBySpecification(Specification<Review> spec);

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review.
     */
    @Transactional
    Review addReview(Review newReview);

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review.
     */
    @Transactional
    Boolean deleteReview(Long reviewId);

    /**
     * Updates an existing review.
     *
     * @param updatedReview The updated review.
     * @return The updated review.
     */
    @Transactional
    Review updateReview(Review updatedReview);

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review.
     */
    @Transactional
    Optional<Review> getReview(Long reviewId);

    /**
     * Retrieves a list of all reviews written by a specific user identified by the given user ID.
     *
     * @param userId the ID of the user whose reviews to retrieve (required)
     * @return a list of Review objects for the given user, or an empty list if no reviews are found
     */
    @Transactional
    List<Review> getAllReviewsByUserId(Long userId);

    /**
     * Retrieves a list of all reviews for a specific movie identified by the given movie ID.
     *
     * @param movieId the ID of the movie whose reviews to retrieve (required)
     * @return a list of Review objects for the given movie, or an empty list if no reviews are found
     */
    @Transactional
    List<Review> getAllReviewsByMovieId(Long movieId);

}
