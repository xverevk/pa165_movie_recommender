package cz.muni.fi.pa165.usermanagement.rest;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
public class ApiError {

    private LocalDateTime timestamp;
    private HttpStatus status;
    private String message;
    private String path;

    public ApiError(HttpStatus status, String message, String path) {
        this.timestamp = LocalDateTime.now(Clock.systemUTC());
        this.status = status;
        this.message = message;
        this.path = path;
    }

    @Override
    public String toString() {
        return "ApiError{" +
            "timestamp=" + timestamp +
            ", status=" + status +
            ", message='" + message + '\'' +
            ", path='" + path + '\'' +
            '}';
    }
}
