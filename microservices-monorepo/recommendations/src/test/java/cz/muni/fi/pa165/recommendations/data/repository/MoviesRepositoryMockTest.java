package cz.muni.fi.pa165.recommendations.data.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
class MoviesRepositoryMockTest {

    // prepared for next milestone

    @Autowired
    private MoviesRepositoryMock moviesRepository;

    @Autowired
    private TestEntityManager testEntityManager;

}
