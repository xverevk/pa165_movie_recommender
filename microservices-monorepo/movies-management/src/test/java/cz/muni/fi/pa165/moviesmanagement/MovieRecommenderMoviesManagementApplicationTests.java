package cz.muni.fi.pa165.moviesmanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MovieRecommenderMoviesManagementApplicationTests {

	@Test
	void contextLoads() {
		// Intentionally empty
	}

}
