package cz.muni.fi.pa165.recommendations.service;

import cz.muni.fi.pa165.recommendations.data.enums.MovieGenre;
import cz.muni.fi.pa165.recommendations.data.model.Movie;
import cz.muni.fi.pa165.recommendations.data.repository.MoviesRepositoryMock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Comparator;

@Service
public class RecommendationsService {

	/**
	 * Default number of recommended movies
	 */
	public static final int DEFAULT_RECOMMENDATIONS = 10;
	private final MoviesRepositoryMock moviesRepositoryMock;

	/**
	 * RecommendationsService constructor
	 *
	 * @param moviesRepositoryMock movies repository class
	 */
	@Autowired
	public RecommendationsService(final MoviesRepositoryMock moviesRepositoryMock) {
		this.moviesRepositoryMock = moviesRepositoryMock;
	}

	/**
	 * Get 10 recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @return List of recommended movies
	 */
	@Transactional(readOnly = true)
	public List<Movie> getRecommendedMovies(final Long movieId) {
		return calculateRecommendedMovies(movieId, DEFAULT_RECOMMENDATIONS);
	}

	/**
	 * Get parametrized number of recommended movies for a given movie
	 *
	 * @param movieId ID of the movie
	 * @param count   number of recommended movies to return
	 * @return List of recommended movies
	 */
	@Transactional(readOnly = true)
	public List<Movie> getRecommendedMovies(final Long movieId, final int count) {
		return calculateRecommendedMovies(movieId, count);
	}

	/**
	 * Calculate recommended movies for a given movie
	 * Recommended movies are chosen by genre similarity and rating
	 *
	 * @param movieId ID of the movie
	 * @param count   number of recommended movies to return
	 * @return List of recommended movies
	 */
	private List<Movie> calculateRecommendedMovies(final Long movieId, final int count) {
		Movie selectedMovie = moviesRepositoryMock.findById(movieId)
				.orElseThrow(() -> new RuntimeException("Movie with id=" + movieId + " doesn't exist"));

		List<Movie> recommendedMovies = new ArrayList<>(moviesRepositoryMock.getAll());
		recommendedMovies.remove(selectedMovie);

		Comparator<Movie> genreComparator = Comparator.comparingDouble(
				movie -> calculateGenreSimilarity(selectedMovie, movie)
		);
		Comparator<Movie> ratingComparator = Comparator.comparingDouble(Movie::getRating);

		recommendedMovies.sort(genreComparator.reversed().thenComparing(ratingComparator.reversed()));

		return recommendedMovies.subList(0, Math.min(count, recommendedMovies.size()));
	}

	/**
	 * Calculate similarity of genres between two movies
	 *
	 * @param movie1 first movie
	 * @param movie2 second movie
	 * @return similarity of genres
	 */
	private double calculateGenreSimilarity(final Movie movie1, final Movie movie2) {
		Set<MovieGenre> genres1 = new HashSet<>(movie1.getGenres());
		Set<MovieGenre> genres2 = new HashSet<>(movie2.getGenres());
		genres1.retainAll(genres2);
		return (double) genres1.size() / (genres1.size() + genres2.size());
	}
}
