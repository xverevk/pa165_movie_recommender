package cz.muni.fi.pa165.usermanagement.testutilities;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import cz.muni.fi.pa165.usermanagement.api.UserDTO;

import java.io.IOException;
import java.util.List;

public final class ObjectConverter {

    private ObjectConverter() {
    }

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
        .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        .registerModule(new JavaTimeModule())
        .setPropertyNamingStrategy(new PropertyNamingStrategies.LowerCamelCaseStrategy())
        .setSerializationInclusion(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL);

    public static <T> T convertJsonToObject(String serializedObject, Class<T> objectClass) throws JsonProcessingException {
        return OBJECT_MAPPER.readValue(serializedObject, objectClass);
    }

    public static String convertObjectToJson(Object object) throws JsonProcessingException {
        return OBJECT_MAPPER.writeValueAsString(object);
    }

    public static <T> T convertJsonToObject(String object, TypeReference<T> typeReference) throws IOException {
        return OBJECT_MAPPER.readValue(object, typeReference);
    }

    public static List<UserDTO> convertJsonToListOfUserDTO(String json) throws IOException {
        TypeReference<List<UserDTO>> typeReference = new TypeReference<>() {
        };
        return OBJECT_MAPPER.readValue(json, typeReference);
    }
}
