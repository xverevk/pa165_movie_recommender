package cz.muni.fi.pa165.moviesmanagement.utils;

import cz.muni.fi.pa165.moviesmanagement.dto.ReviewDTO;
import cz.muni.fi.pa165.moviesmanagement.enums.FilmProperty;

import java.util.Map;

public class ReviewMockDataFactory {

    public static ReviewDTO createGoodReviewDto(Long movieId, Long userId) {
        Map<FilmProperty, Float> ratings = Map.of(
                FilmProperty.PLOT, 10.0f,
                FilmProperty.ACTING, 10.0f,
                FilmProperty.SOUND, 10.0f,
                FilmProperty.SPECIAL_EFFECTS, 10.0f,
                FilmProperty.COSTUMES, 10.0f
        );
        ReviewDTO reviewDTO = new ReviewDTO();
        reviewDTO.setMovieId(movieId);
        reviewDTO.setUserId(userId);
        reviewDTO.setId(1L);
        reviewDTO.setRatings(ratings);
        reviewDTO.setOverallRating(10.0);
        return reviewDTO;
    }

    public static ReviewDTO createBadReviewDto(Long movieId, Long userId) {
        Map<FilmProperty, Float> ratings = Map.of(
                FilmProperty.PLOT, 1.0f,
                FilmProperty.ACTING, 1.0f,
                FilmProperty.SOUND, 1.0f,
                FilmProperty.SPECIAL_EFFECTS, 1.0f,
                FilmProperty.COSTUMES, 1.0f
        );
        ReviewDTO reviewDTO = new ReviewDTO();
        reviewDTO.setMovieId(movieId);
        reviewDTO.setUserId(userId);
        reviewDTO.setId(1L);
        reviewDTO.setRatings(ratings);
        reviewDTO.setOverallRating(1.0);
        return reviewDTO;
    }

    public static ReviewDTO createAverageReviewDto(Long movieId, Long userId) {
        Map<FilmProperty, Float> ratings = Map.of(
                FilmProperty.PLOT, 5.0f,
                FilmProperty.ACTING, 5.0f,
                FilmProperty.SOUND, 5.0f,
                FilmProperty.SPECIAL_EFFECTS, 5.0f,
                FilmProperty.COSTUMES, 5.0f
        );
        ReviewDTO reviewDTO = new ReviewDTO();
        reviewDTO.setMovieId(movieId);
        reviewDTO.setUserId(userId);
        reviewDTO.setId(1L);
        reviewDTO.setRatings(ratings);
        reviewDTO.setOverallRating(5.0);
        return reviewDTO;
    }
}
