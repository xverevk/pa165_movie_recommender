package cz.muni.fi.pa165.moviesmanagement.rest;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import cz.muni.fi.pa165.moviesmanagement.utils.MovieMockDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

class MovieControllerTest {

    @Mock
    private MovieFacade movieFacade;

    @InjectMocks
    private MovieController movieController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void find_noParameters_returnsMovieDtoCollection() {
        // Arrange
        Collection<MovieDto> expectedMovies = getSampleMovieData();
        when(movieFacade.find(null, null, null)).thenReturn(expectedMovies);

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(null, null, null);

        // Assert
        assertEquals(expectedMovies, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void find_byValidId_returnsMovieDto() {
        // Arrange
        long id = 1L;
        MovieDto expectedMovie = new MovieDto();
        when(movieFacade.find(id, null, null)).thenReturn(Collections.singletonList(expectedMovie));

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(id, null, null);

        // Assert
        assertEquals(Collections.singletonList(expectedMovie), response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void find_byInvalidId_returnsNotFoundStatus() {
        // Arrange
        long id = 1L;
        when(movieFacade.find(id, null, null)).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(id, null, null);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void find_byValidName_returnsMovieDtoCollection() {
        // Arrange
        String name = "Test Movie";
        MovieDto expectedMovie = new MovieDto();
        when(movieFacade.find(null, name, null)).thenReturn(Collections.singletonList(expectedMovie));

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(null, name, null);

        // Assert
        assertEquals(Collections.singletonList(expectedMovie), response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void find_byInvalidName_returnsNotFoundStatus() {
        // Arrange
        String name = "Non-existent Movie";
        when(movieFacade.find(null, name, null)).thenReturn(Collections.emptyList());

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(null, name, null);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void find_byValidGenre_returnsMovieDtoCollection() {
        // Arrange
        MovieGenre genre = MovieGenre.ACTION;
        Collection<MovieDto> expectedMovies = new ArrayList<>();
        when(movieFacade.find(null, null, genre)).thenReturn(expectedMovies);

        // Act
        ResponseEntity<Collection<MovieDto>> response = movieController.find(null, null, genre);

        // Assert
        assertNull(response.getBody());
    }

    @Test
    void addMovie_allCorrect_returnMovieDto() {
        // Arrange
        CreateMovieDto createMovieDto = new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "Test Description",
                new HashSet<>(Collections.singletonList(MovieGenre.ACTION)),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );
        MovieDto addedMovie = new MovieDto();
        when(movieFacade.addMovie(createMovieDto)).thenReturn(addedMovie);

        // Act
        MovieDto result = movieController.addMovie(createMovieDto).getBody();

        // Assert
        assertEquals(addedMovie, result);
    }

    @Test
    void updateMovie_allCorrect_returnsNewMovieDto() {
        // Arrange
        MovieDto movieDto = new MovieDto();
        MovieDto updatedMovie = new MovieDto();
        when(movieFacade.updateMovie(movieDto)).thenReturn(updatedMovie);

        // Act
        MovieDto result = movieController.updateMovie(movieDto).getBody();

        // Assert
        assertEquals(updatedMovie, result);
    }

    @Test
    void deleteMovie_allCorrect_returnsOk() {
        // Arrange
        long id = 1L;
        when(movieFacade.deleteMovie(id)).thenReturn(true);

        // Act
        ResponseEntity<Boolean> response = movieController.deleteMovie(id);

        // Assert
        assertEquals(Boolean.TRUE, response.getBody());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void deleteMovie_notFoundId_returnsNotFoundStatus() {
        // Arrange
        long id = 1L;
        when(movieFacade.deleteMovie(id)).thenReturn(false);

        // Act
        ResponseEntity<Boolean> response = movieController.deleteMovie(id);

        // Assert
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }

    @Test
    void getUsersFavoriteMovies_allCorrect_returnsMovieDtoCollection() {
        // Arrange
        long userId = 1L;
        List<MovieDto> expectedMovies = MovieMockDataFactory.createAllMockMoviesWithIdsAsDto();
        when(movieFacade.getUsersFavoriteMovies(userId)).thenReturn(expectedMovies);

        // Act
        Collection<MovieDto> actualMovies = movieController.getUsersFavoriteMovies(userId).getBody();

        // Assert
        assertEquals(expectedMovies, actualMovies);
    }

    public Collection<MovieDto> getSampleMovieData() {
        Collection<MovieDto> movies = new ArrayList<>();
        movies.add(new MovieDto());
        movies.add(new MovieDto());
        movies.add(new MovieDto());
        return movies;
    }

}
