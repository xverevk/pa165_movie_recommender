package cz.muni.fi.pa165.ratingsandreviews.specification;

import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import jakarta.persistence.criteria.Expression;
import jakarta.persistence.criteria.MapJoin;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

public final class ReviewSpecification {

    /**
     * Default constructor
     */
    private ReviewSpecification() { }

    /**
     * Creates a Specification for ordering reviews by their overall rating.
     *
     * This method constructs a Specification that can be used with JPA criteria queries
     * to order reviews based on their overall rating.
     *
     * @param ascending true to order reviews in ascending order (lowest to highest) by overall rating,
     *                  false to order in descending order (highest to lowest)
     * @return a Specification that orders reviews by overall rating
     */
    public static Specification<Review> orderByOverallRating(boolean ascending) {
        return (root, query, criteriaBuilder) -> {
            query.orderBy(ascending ? criteriaBuilder.asc(root.get("overallRating"))
                    : criteriaBuilder.desc(root.get("overallRating")));
            return criteriaBuilder.conjunction();
        };
    }

    /**
     * Creates a Specification for ordering reviews by their rating for a specific film property.
     *
     * This method constructs a Specification that can be used with JPA criteria queries
     * to order reviews based on the rating given to a specific film property (e.g., acting, directing).
     *
     * @param property the FilmProperty for which to order reviews by rating
     * @param ascending true to order reviews in ascending order (lowest to highest) by rating,
     *                  false to order in descending order (highest to lowest)
     * @return a Specification that orders reviews by rating for a specific film property
     */
    public static Specification<Review> orderByRatingForProperty(FilmProperty property, boolean ascending) {
        return (root, query, criteriaBuilder) -> {
            MapJoin<Review, FilmProperty, Float> ratingsJoin = root.joinMap("ratings");
            Predicate propertyPredicate = criteriaBuilder.equal(ratingsJoin.key(), property);
            Expression<Float> ratingExpression = ratingsJoin.value();
            query.orderBy(ascending ? criteriaBuilder.asc(ratingExpression) : criteriaBuilder.desc(ratingExpression));
            return propertyPredicate;
        };
    }
}
