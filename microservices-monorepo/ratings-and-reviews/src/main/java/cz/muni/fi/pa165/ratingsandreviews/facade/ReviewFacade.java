package cz.muni.fi.pa165.ratingsandreviews.facade;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public interface ReviewFacade {

    /**
     * Adds a new review.
     *
     * @param newReview The review to be added.
     * @return The added review DTO.
     */
    ReviewDTO addReview(ReviewDTO newReview);

    /**
     * Deletes a review by its ID.
     *
     * @param reviewId The ID of the review to be deleted.
     * @return The deleted review DTO.
     */
    Boolean deleteReview(Long reviewId);

    /**
     * Updates a review.
     *
     * @param updatedReview The updated review.
     * @return The updated review DTO.
     */
    ReviewDTO updateReview(ReviewDTO updatedReview);

    /**
     * Retrieves a review by its ID.
     *
     * @param reviewId The ID of the review to be retrieved.
     * @return The retrieved review DTO.
     */
    Optional<ReviewDTO> getReview(Long reviewId);

    /**
     * Retrieves reviews by user ID.
     *
     * @param userId The ID of the user whose reviews are to be retrieved.
     * @return List of review DTOs.
     */
    List<ReviewDTO> getReviewsByUserId(Long userId);

    /**
     * Retrieves reviews by movie ID.
     *
     * @param movieId the ID of the movie
     * @return list of review DTOs by the movie
     */
    List<ReviewDTO> getReviewsByMovieId(Long movieId);

    /**
     * Retrieves reviews sorted by specifications
     *
     * @param spec specification for sorting
     * @return List of sorted reviews
     */
    List<ReviewDTO> getReviewsBySpecification(Specification<Review> spec);
}
