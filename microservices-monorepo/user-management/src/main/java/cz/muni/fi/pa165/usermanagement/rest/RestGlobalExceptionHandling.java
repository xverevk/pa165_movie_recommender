package cz.muni.fi.pa165.usermanagement.rest;

import jakarta.persistence.EntityNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.util.UrlPathHelper;

@RestControllerAdvice
public class RestGlobalExceptionHandling {

    private static final UrlPathHelper URL_PATH_HELPER = new UrlPathHelper();

    @ExceptionHandler({EntityNotFoundException.class})
    public ResponseEntity<Object> handleResourceNotFound(final EntityNotFoundException exception,
                                                         final HttpServletRequest request) {
        return createResponseEntity(exception, request, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DataIntegrityViolationException.class})
    public ResponseEntity<Object> handleResourceNotFound(final DataIntegrityViolationException exception,
                                                         final HttpServletRequest request) {
        return createResponseEntity(exception, request, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception exception, final HttpServletRequest request) {
        return createResponseEntity(getInitialException(exception), request, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> createResponseEntity(final Exception ex, final HttpServletRequest request,
                                                        HttpStatus status) {
        ApiError apiError = new ApiError(status,
            ex.getLocalizedMessage(),
            URL_PATH_HELPER.getRequestUri(request));
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    private Exception getInitialException(Exception ex) {
        while (ex.getCause() != null) {
            ex = (Exception) ex.getCause();
        }
        return ex;
    }
}
