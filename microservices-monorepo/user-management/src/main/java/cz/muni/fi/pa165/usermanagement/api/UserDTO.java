package cz.muni.fi.pa165.usermanagement.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;


@Schema(title = "UserDTO",
    description = "represents a standard user DTO object"
)
public record UserDTO(
    @NotBlank
    @Schema(description = "id", example = "5445646510544")
    long id,

    @NotBlank
    @Schema(description = "username", example = "user123456")
    @JsonProperty("userName")
    String userName,

    @NotBlank
    @Schema(description = "email", example = "user123456@gmail.com")
    @JsonProperty("email")
    String email,

    @NotBlank
    @Schema(description = "user type", example = "USER")
    @JsonProperty("userType")
    UserType userType
) {
}
