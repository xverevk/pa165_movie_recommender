package cz.muni.fi.pa165.moviesmanagement.service;

import cz.muni.fi.pa165.moviesmanagement.dto.ReviewDTO;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import cz.muni.fi.pa165.moviesmanagement.service.api.RatingsApiService;
import cz.muni.fi.pa165.moviesmanagement.utils.MovieMockDataFactory;
import cz.muni.fi.pa165.moviesmanagement.utils.ReviewMockDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.jpa.domain.Specification;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class MovieServiceTest {

    @Mock
    private MovieRepository movieRepository;

    @Mock
    RatingsApiService ratingsApiService;

    @InjectMocks
    private MovieService movieService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAllMovies_allCorrect_returnsMoviesCollection() {
        // Arrange
        List<Movie> movies = Arrays.asList(
                new Movie(),
                new Movie()
        );
        when(movieRepository.findAll(any(Specification.class))).thenReturn(movies);

        // Act
        Collection<Movie> result = movieService.find(null, null, null);

        // Assert
        assertEquals(movies.size(), result.size());
        assertTrue(result.containsAll(movies));
    }

    @Test
    void findById_allCorrect_returnsMovie() {
        // Arrange
        Long id = 1L;
        Movie movie = new Movie();
        when(movieRepository.findAll(any(Specification.class))).thenReturn(Collections.singletonList(movie));
        // Act
        Collection<Movie> result = movieService.find(id, null, null);

        // Assert
        assertTrue(result.contains(movie));
        assertEquals(1, result.size());
    }

    @Test
    void findById_notFoundId_returnsEmpty() {
        // Arrange
        Long id = 999L;
        when(movieRepository.findById(id)).thenReturn(Optional.empty());

        // Act
        Collection<Movie> result = movieService.find(id, null, null);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void findByGenre_allCorrect_returnsMovies() {
        // Arrange
        MovieGenre genre = MovieGenre.ACTION;
        Set<MovieGenre> genres = Collections.singleton(genre);
        List<Movie> movies = Arrays.asList(
                new Movie(),
                new Movie()
        );
        when(movieRepository.findAll(any(Specification.class))).thenReturn(movies);

        // Act
        Collection<Movie> result = movieService.find(null, null, genres);

        // Assert
        assertEquals(movies.size(), result.size());
        assertTrue(result.containsAll(movies));
    }

    @Test
    void addMovie_allCorrect_returnsMovie() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.add(movie);

        // Assert
        verify(movieRepository, times(1)).save(movie);
    }

    @Test
    void deleteMovie_allCorrect() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.delete(movie);

        // Assert
        verify(movieRepository, times(1)).delete(movie);
    }

    @Test
    void updateMovie_allCorrect() {
        // Arrange
        Movie movie = new Movie();

        // Act
        movieService.update(movie);

        // Assert
        verify(movieRepository, times(1)).save(movie);
    }

    @Test
    void getUsersFavoriteMovies_allCorrect_returnsMovies() {
        // Arrange
        Long userId = 1L;
        Movie shawshank = MovieMockDataFactory.createShawshankWithId(1L);
        Movie godfather = MovieMockDataFactory.createGodfatherWithId(2L);
        Movie bastards1 = MovieMockDataFactory.createBastardi1WithId(3L);

        ReviewDTO godfatherReview = ReviewMockDataFactory.createAverageReviewDto(godfather.getId(), userId);
        ReviewDTO shawshankReview = ReviewMockDataFactory.createGoodReviewDto(shawshank.getId(), userId);
        ReviewDTO bastards1Review = ReviewMockDataFactory.createBadReviewDto(bastards1.getId(), userId);

        when(movieRepository.findAll()).thenReturn(Arrays.asList(shawshank, godfather, bastards1));
        when(ratingsApiService.getReviewsByUserId(userId)).thenReturn(Arrays.asList(shawshankReview, godfatherReview, bastards1Review));

        // Act
        List<Movie> result = movieService.getUsersFavoriteMovies(userId);

        // Assert
        assertEquals(3, result.size());
        // assert that order is from best to worst
        assertEquals(shawshank, result.get(0));
        assertEquals(godfather, result.get(1));
        assertEquals(bastards1, result.get(2));
    }
}
