package cz.muni.fi.pa165.ratingsandreviews.utils;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public final class TestDataFactory {

    private TestDataFactory() {
    }

    /**
     * Creates a single review instance.
     *
     * @param id The ID of the review.
     * @return A Movie object.
     */
    public static Review createReview(Long id) {
        return new Review(
                id,
                1L + id,
                1L + id,
                1L,
                null
        );
    }

    /**
     * Creates a list of review instances for testing, with a specific count.
     *
     * @param count The number of review to create.
     * @return A list of review objects.
     */
    public static List<Review> createReviewList(int count) {
        List<Review> movies = new ArrayList<>();
        for (long i = 1; i <= count; i++) {
            movies.add(createReview(i));
        }
        return movies;
    }

    /**
     * Creates a list of MovieDTO instances for testing, with a specific count.
     *
     * @param count The number of MovieDTOs to create.
     * @return A list of MovieDTO objects.
     */
    public static List<ReviewDTO> createReviewDTOList(int count) {
        List<ReviewDTO> movieDTOs = new ArrayList<>();
        for (long i = 1; i <= count; i++) {
            Review review = createReview(i);
            movieDTOs.add(new ReviewDTO(
                    review.getMovieId(),
                    review.getUserId(),
                    review.getId(),
                    null,
                    review.getOverallRating()
            ));
        }
        return movieDTOs;
    }

}
