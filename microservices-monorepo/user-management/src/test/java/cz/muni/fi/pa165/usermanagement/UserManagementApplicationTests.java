package cz.muni.fi.pa165.usermanagement;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest(classes = UserManagementApplication.class)
class UserManagementApplicationTests {

    @Test
    void contextLoads() {
        // intentionally empty
    }

}
