package cz.muni.fi.pa165.moviesmanagement.service.api;

import cz.muni.fi.pa165.moviesmanagement.dto.ReviewDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Service
public class RatingsApiService {

    private final WebClient client;

    /**
     * Constructor for RatingsApiService.
     * @param baseUrl The base URL of the ratings and reviews service.
     */
    public RatingsApiService(@Value("${service.ratings-and-reviews.url}") final String baseUrl) {
        this.client = WebClient.create(baseUrl);
    }

    /**
     * Retrieves all reviews.
     * @param userId The identifier of the movie.
     * @return A list of all review DTOs.
     */
    public List<ReviewDTO> getReviewsByUserId(final Long userId) {
        return client.get()
                .uri(uriBuilder -> uriBuilder
                        .path("/api/reviews/getByUserId")
                        .queryParam("userId", userId)
                        .build())
                .retrieve()
                .bodyToFlux(ReviewDTO.class)
                .collectList()
                .block();
    }

}
