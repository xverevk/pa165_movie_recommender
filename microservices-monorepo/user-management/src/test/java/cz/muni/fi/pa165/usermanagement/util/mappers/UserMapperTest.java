package cz.muni.fi.pa165.usermanagement.util.mappers;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mapstruct.factory.Mappers;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import org.junit.jupiter.api.Assertions;

@ExtendWith(MockitoExtension.class)
class UserMapperTest {

	/**
	 * The ID of the first created user.
	 */
	public static final long FIRST_USER_ID = 1L;

	private UserMapper userMapper;

	/**
	 * Sets up the UserMapper before each test.
	 */
	@BeforeEach
	void setUp() {
		userMapper = Mappers.getMapper(UserMapper.class);
	}

	/**
	 * Test method for {@link UserMapper#userToUserDTO(User)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void userToUserDTO(final UserType userType) {
		User user = TestDataFactory.createUser(FIRST_USER_ID, userType);

		UserDTO userDTO = userMapper.userToUserDTO(user);

		Assertions.assertEquals(user.getId(), userDTO.id());
		Assertions.assertEquals(user.getUserName(), userDTO.userName());
		Assertions.assertEquals(user.getEmail(), userDTO.email());
		Assertions.assertEquals(user.getUserType(), userDTO.userType());
	}

	/**
	 * Test method for {@link UserMapper#usersToUserDTOs(List)}.
	 */
	@Test
	void usersToUserDTOs() {
		List<User> users = TestDataFactory.createUserMultiple(5L);

		List<UserDTO> userDTOs = userMapper.usersToUserDTOs(users);

		Assertions.assertEquals(users.size(), userDTOs.size());
		for (int i = 0; i < users.size(); i++) {
			Assertions.assertEquals(users.get(i).getId(), userDTOs.get(i).id());
			Assertions.assertEquals(users.get(i).getUserName(), userDTOs.get(i).userName());
			Assertions.assertEquals(users.get(i).getEmail(), userDTOs.get(i).email());
			Assertions.assertEquals(users.get(i).getUserType(), userDTOs.get(i).userType());
		}
	}

	/**
	 * Test method for {@link UserMapper#userRegistrationDTOToUser(UserRegistrationDTO)}.
	 */
	@ParameterizedTest
	@EnumSource(UserType.class)
	void userRegistrationDTOToUser(final UserType userType) {
		UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO(
				"testUser", userType
		);

		User user = userMapper.userRegistrationDTOToUser(userRegistrationDTO);

		Assertions.assertEquals(userRegistrationDTO.userName(), user.getUserName());
		Assertions.assertEquals(userRegistrationDTO.email(), user.getEmail());
		Assertions.assertEquals(userRegistrationDTO.encryptedPassword(), user.getEncryptedPassword());
		Assertions.assertEquals(userRegistrationDTO.userType(), userType);
	}

	/**
	 * Test method for {@link UserMapper#updateEntityWithDto(UserUpdateDTO, User)}.
	 */
	@Test
	void updateEntityWithDto() {
		User user = TestDataFactory.createUser(FIRST_USER_ID, UserType.USER);
		UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("testUser", UserType.ADMIN);

		userMapper.updateEntityWithDto(userUpdateDTO, user);

		Assertions.assertEquals(userUpdateDTO.userName(), user.getUserName());
		Assertions.assertEquals(userUpdateDTO.email(), user.getEmail());
		Assertions.assertEquals(userUpdateDTO.encryptedPassword(), user.getEncryptedPassword());
		Assertions.assertEquals(userUpdateDTO.userType(), user.getUserType());
	}
}
