package cz.muni.fi.pa165.ratingsandreviews.utils;

import cz.muni.fi.pa165.ratingsandreviews.model.FilmProperty;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class ReviewMockDataFactory {

    private static final Random random = new Random();
    public static Review createReview(Long userId, Long movieId) {
        Review review = new Review();
        review.setUserId(userId);
        review.setMovieId(movieId);
        review.setRatings(getRandomRatings());
        review.setOverallRating(getRandomOverallRating());
        return review;
    }

    private static HashMap<FilmProperty, Float> getRandomRatings() {
        HashMap<FilmProperty, Float> ratingsMap = new HashMap<>();
        for (FilmProperty property : FilmProperty.values()) {
            float randomRating = (float) (Math.round(random.nextFloat() * 100.0f) / 10.0f);
            ratingsMap.put(property, randomRating);
        }

        return ratingsMap;
    }

    private static float getRandomOverallRating() {
        float randomN = random.nextFloat()  * 10.0f;
        return (float) (Math.round(randomN));
    }

    public static List<Review> mockAllReviews() {
        List<Review> reviews = new ArrayList<>();
        for (long userId = 1; userId <= 5; userId++) {
            for (long movieId = 1; movieId <= 5; movieId++) {
                Review review = createReview(userId, movieId);
                reviews.add(review);
            }
        }

        return reviews;
    }


}

