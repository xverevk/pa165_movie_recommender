package cz.muni.fi.pa165.recommendations.data.repository;

import cz.muni.fi.pa165.recommendations.data.model.Movie;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Optional;
import java.util.ArrayList;

@Repository
public class MoviesRepositoryMock {

	private final Map<Long, Movie> movies = new HashMap<>();

	/**
	 * Returns all movies
	 *
	 * @return list of movies
	 */
	public List<Movie> getAll() {
		return new ArrayList<>(movies.values());
	}

	/**
	 * Returns movie by its id
	 *
	 * @param id movie id
	 * @return movie
	 */
	public Optional<Movie> findById(final Long id) {
		return Optional.ofNullable(movies.get(id));
	}

	/**
	 * Adds new movie to repository
	 *
	 * @param movie movie
	 */
	public void add(final Movie movie) {
		movies.put(movie.getId(), movie);
	}

	/**
	 * Deletes movie from repository
	 *
	 * @param movie movie
	 */
	public void delete(final Movie movie) {
		movies.remove(movie.getId());
	}

	/**
	 * Updates movie in repository
	 *
	 * @param movie movie
	 */
	public void update(final Movie movie) {
		movies.put(movie.getId(), movie);
	}
}
