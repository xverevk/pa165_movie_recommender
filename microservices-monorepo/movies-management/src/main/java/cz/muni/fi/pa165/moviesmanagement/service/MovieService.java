package cz.muni.fi.pa165.moviesmanagement.service;

import cz.muni.fi.pa165.moviesmanagement.dto.ReviewDTO;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.model.MovieSpecification;
import cz.muni.fi.pa165.moviesmanagement.repository.MovieRepository;
import cz.muni.fi.pa165.moviesmanagement.service.api.RatingsApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MovieService {

    private final MovieRepository movieRepository;
    private final RatingsApiService ratingsApiService;

    /**
     * Constructor for MovieService.
     *
     * @param movieRepository   The movie repository.
     * @param ratingsApiService The ratings API service.
     */
    @Autowired
    public MovieService(final MovieRepository movieRepository, final RatingsApiService ratingsApiService) {
        this.movieRepository = movieRepository;
        this.ratingsApiService = ratingsApiService;
    }

    /**
     * Retrieves movies based on a specification.
     * @param id The identifier of the movie.
     * @param name The name of the movie.
     * @param genres The genres of the movie.
     * @return A collection of movies of the specified by the parameter.
     */
    @Transactional(readOnly = true)
    public Collection<Movie> find(Long id, String name, Set<MovieGenre> genres) {
        Specification<Movie> spec = MovieSpecification.buildSearchSpecification(id, name, genres);
        return movieRepository.findAll(spec);
    }

    /**
     * Adds a new movie to the repository.
     *
     * @param movie The movie to be added.
     */
    @Transactional
    public void add(final Movie movie) {
        movieRepository.save(movie);
    }

    /**
     * Deletes a movie from the repository.
     *
     * @param movie The movie to be deleted.
     */
    @Transactional
    public void delete(final Movie movie) {
        movieRepository.delete(movie);
    }

    /**
     * Updates a movie in the repository.
     *
     * @param movie The movie to be updated.
     */
    @Transactional
    public void update(final Movie movie) {
        movieRepository.save(movie);
    }

    /**
     * Retrieves all movies that a user has reviewed.
     *
     * @param userId The identifier of the user.
     * @return A collection of movies reviewed by the user.
     */
    public List<Movie> getUsersFavoriteMovies(final Long userId) {
        List<ReviewDTO> reviews = ratingsApiService.getReviewsByUserId(userId);
        List<Movie> movies = movieRepository.findAll();

        // Map of movie IDs to overall ratings
        Map<Long, Double> movieRatings = reviews.stream()
                .collect(Collectors.groupingBy(ReviewDTO::getMovieId,
                        Collectors.averagingDouble(ReviewDTO::getOverallRating)));

        // Sort movies based on the average ratings from best to worst
        return movies.stream()
                // Filter out movies with no reviews
                .filter(movie -> movieRatings.containsKey(movie.getId()))
                // Sort descending
                .sorted((m1, m2) -> movieRatings.get(m2.getId()).compareTo(movieRatings.get(m1.getId())))
                .collect(Collectors.toList());

    }

}
