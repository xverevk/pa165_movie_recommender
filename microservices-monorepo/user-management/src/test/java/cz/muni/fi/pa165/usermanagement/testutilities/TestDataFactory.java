package cz.muni.fi.pa165.usermanagement.testutilities;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings({"PMD", "checkstyle:hideutilityclassconstructor"})  // spring triggers linters false-positives
public class TestDataFactory {

	private static final String testEmailStart = "test-email";
	private static final String testEmailEnd = "@gmail.com";
	private static final String testUsername = "test-user";
	private static final String testPassword = "test-password";
	private static final UserType testUserType = UserType.USER;

	public static User createUser(final long id, final UserType userType) {
		User user = new User();
		user.setId(id);
		user.setUserName(testUsername + id);
		user.setEmail(testEmailStart + id + testEmailEnd);
		user.setEncryptedPassword(testPassword + id);
		user.setUserType(userType);
		return user;
	}

	public static List<User> createUsersToStore(final int count, final UserType userType) {
		List<User> users = new ArrayList<>();
		for (int i = 0; i < count; i++) {
			users.add(createUserToStore(userType));
		}
		return users;
	}

	public static User createUserToStore(final UserType userType) {
		User user = new User();
		long uniqueSuffix = System.nanoTime();
		user.setUserName(testUsername + uniqueSuffix);
		user.setEmail(testEmailStart + uniqueSuffix + testEmailEnd);
		user.setEncryptedPassword(testPassword + uniqueSuffix);
		user.setUserType(userType);
		return user;
	}

	public static User createUserToStoreWithEmail(String email) {
		User user = createUserToStore(testUserType);
		user.setEmail(email);
		return user;
	}

	public static User createUserToStoreWithUsername(String username) {
		User user = createUserToStore(testUserType);
		user.setUserName(username);
		return user;
	}

	public static List<User> createUserMultiple(final long count) {
		List<User> users = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			users.add(createUser(i, testUserType));
		}
		return users;
	}

	public static List<User> createUserMultiple(final long count, UserType userType) {
		List<User> users = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			users.add(createUser(i, userType));
		}
		return users;
	}

	public static UserDTO createUserDTO(final long id, final UserType userType) {
		return new UserDTO(id, testUsername + id, testEmailStart + id  + testEmailEnd, userType);
	}

	public static List<UserDTO> createUserDTOMultiple(final long count) {
		List<UserDTO> usersDTO = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			usersDTO.add(createUserDTO(i, testUserType));
		}
		return usersDTO;
	}

	public static List<UserDTO> createUserDTOMultiple(final long count, UserType userType) {
		List<UserDTO> usersDTO = new ArrayList<>();
		for (long i = 1; i <= count; i++) {
			usersDTO.add(createUserDTO(i, userType));
		}
		return usersDTO;
	}

	public static UserRegistrationDTO createUserRegistrationDTO(final String identifier, final UserType userType) {
		return new UserRegistrationDTO(
				identifier, identifier + "@gmail.com", identifier + "-password", userType
		);
	}

	/**
	 * Creates a UserUpdateDTO with the given identifier and UserType.
	 *
	 * @param identifier The identifier of the UserUpdateDTO.
	 * @param userType   The UserType of the UserUpdateDTO.
	 * @return The created UserUpdateDTO.
	 */
	public static UserUpdateDTO createUserUpdateDTO(final String identifier, final UserType userType) {
		return new UserUpdateDTO(
				identifier, identifier + "@gmail.com", identifier + "-password", userType
		);
	}

	public static UserDTO createUserDTOWithEmail(String mail) {
		return new UserDTO(1L, testUsername + 1L, mail, UserType.USER);
	}

	public static UserDTO createUserDTOWithUsername(String validUsername) {
		return new UserDTO(1L, validUsername, testEmailStart + 1L + testEmailEnd, UserType.USER);
	}

	public static User createUserWithEmail(String mail) {
		User user = createUser(1L, testUserType);
		user.setEmail(mail);
		return user;
	}

	public static User createUserWithUsername(String validUsername) {
		User user = createUser(1L, testUserType);
		user.setUserName(validUsername);
		return user;
	}
}
