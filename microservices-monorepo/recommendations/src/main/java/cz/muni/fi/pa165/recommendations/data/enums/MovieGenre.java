package cz.muni.fi.pa165.recommendations.data.enums;

import java.util.Locale;

/**
 * Enum representing movie genres.
 */
public enum MovieGenre {
	ACTION,
	ADVENTURE,
	COMEDY,
	DRAMA,
	HORROR,
	ROMANCE,
	SCIFI,
	THRILLER,
	ANIMATION,
	DOCUMENTARY,
	FANTASY,
	MYSTERY,
	CRIME,
	WESTERN,
	HISTORICAL,
	MUSICAL,
	WAR,
	SPORT,
	BIOGRAPHY,
	MUSIC,
	PSYCHOLOGICAL,
	CRIMINAL;

	/**
	 * Returns the capitalized name of the genre.
	 *
	 * @return the capitalized name of the genre
	 */
	public String getCapitalizedName() {
		String lowercaseName = name().toLowerCase(Locale.US);
		return lowercaseName.substring(0, 1).toUpperCase(Locale.US) + lowercaseName.substring(1);
	}
}
