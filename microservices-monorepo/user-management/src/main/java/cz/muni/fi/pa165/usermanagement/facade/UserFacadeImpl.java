package cz.muni.fi.pa165.usermanagement.facade;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.service.UserService;
import cz.muni.fi.pa165.usermanagement.util.mappers.UserMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Primary
@AllArgsConstructor
public class UserFacadeImpl implements UserFacade {

    private final UserMapper userMapper;
    private final UserService userService;

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> getAllUsers(Pageable pageable) {
        return userService.getAllUsers(pageable).map(userMapper::userToUserDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<UserDTO> getAllUsersWithUserType(UserType userType, Pageable pageable) {
        return userService.getAllUsersWithUserType(userType, pageable).map(userMapper::userToUserDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserWithEmail(String email) {
        return userMapper.userToUserDTO(userService.getUserWithEmail(email));
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserWithUsername(String username) {
        return userMapper.userToUserDTO(userService.getUserWithUsername(username));
    }

    @Override
    @Transactional
    public UserDTO createUser(final UserRegistrationDTO userDTO) {
        User user = userMapper.userRegistrationDTOToUser(userDTO);
        return userMapper.userToUserDTO(userService.createUser(user));
    }

    @Override
    @Transactional
    public void updateUser(final long id, final UserUpdateDTO userDTO) {
        User existingUser = userService.getUser(id);
        userMapper.updateEntityWithDto(userDTO, existingUser);
        userService.updateUser(existingUser);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUser(final long id) {
        return userMapper.userToUserDTO(userService.getUser(id));
    }

    @Override
    @Transactional
    public void deleteUser(final long id) {
        userService.deleteUser(id);
    }
}
