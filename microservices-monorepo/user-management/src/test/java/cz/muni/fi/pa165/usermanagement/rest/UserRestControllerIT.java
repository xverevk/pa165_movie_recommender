package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import cz.muni.fi.pa165.usermanagement.testutilities.JsonVerifier;
import cz.muni.fi.pa165.usermanagement.testutilities.KeycloakTokenProvider;
import cz.muni.fi.pa165.usermanagement.testutilities.ObjectConverter;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

@SpringBootTest
@AutoConfigureMockMvc
public class UserRestControllerIT {
/*
    @Autowired
    private MockMvc mockMvc;

    private String accessToken;

    @Autowired
    private KeycloakTokenProvider keycloakTokenProvider;

    @MockBean
    private UserRepository userRepository;

    @BeforeEach
    public void setup() {
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }

    @Test
    public void getAllUsers_NoParams_ReturnsAllUsers() throws Exception {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = TestDataFactory.createUserMultiple(10);
        Page<User> page = new PageImpl<>(users, pageable, users.size());

        Mockito.when(userRepository.findAll(pageable)).thenReturn(page);

        String jsonResponse = mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .param("page", "0")
                .param("size", "10")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        JsonVerifier.verifyJsonResponse(jsonResponse, UserType.USER);
    }

    @Test
    public void getAllUsers_WithUserType_ReturnsFilteredUsers() throws Exception {
        Pageable pageable = PageRequest.of(0, 10);
        List<User> users = TestDataFactory.createUserMultiple(10, UserType.ADMIN);
        Page<User> page = new PageImpl<>(users);

        Mockito.when(userRepository.findAllByUserTypeIs(UserType.ADMIN, pageable)).thenReturn(page);

        String jsonResponse = mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .param("userType", "ADMIN")
                .param("page", "0")
                .param("size", "10")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        JsonVerifier.verifyJsonResponse(jsonResponse, UserType.ADMIN);
    }


    @Test
    public void createUser_ValidUser_ReturnsCreatedUser() throws Exception {
        User newUser = TestDataFactory.createUser(1L, UserType.USER);
        UserDTO newUserDTO = TestDataFactory.createUserDTO(1L, UserType.USER);
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(newUser);

        String requestBody = ObjectConverter.convertObjectToJson(newUser);
        String expectedResponse = ObjectConverter.convertObjectToJson(newUserDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(expectedResponse));
    }

    @Test
    public void getUser_ValidId_ReturnsUser() throws Exception {
        User user = TestDataFactory.createUser(1L, UserType.USER);
        UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);
        Mockito.when(userRepository.findById(user.getId())).thenReturn(Optional.of(user));

        String expectedJson = ObjectConverter.convertObjectToJson(userDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/{id}", user.getId())
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }

    @Test
    public void getUser_InvalidId_ReturnsNotFound() throws Exception {
        Mockito.when(userRepository.findById(999L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/{id}", 999)
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void updateUser_ValidId_ReturnsNoContent() throws Exception {
        User existingUser = TestDataFactory.createUser(1L, UserType.USER);
        Mockito.when(userRepository.findById(existingUser.getId())).thenReturn(Optional.of(existingUser));
        Mockito.when(userRepository.save(Mockito.any(User.class))).thenReturn(existingUser);

        String requestBody = ObjectConverter.convertObjectToJson(existingUser);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", existingUser.getId())
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void updateUser_InvalidId_ReturnsNotFound() throws Exception {
        long invalidId = 999L;
        UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("identifier", UserType.USER);
        String requestBody = ObjectConverter.convertObjectToJson(userUpdateDTO);

        Mockito.when(userRepository.findById(invalidId)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", 999)
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void deleteUser_ValidId_ReturnsNoContent() throws Exception {
        Mockito.doNothing().when(userRepository).deleteById(1L);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/{id}", 1)
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @Test
    public void deleteUser_InvalidId_ReturnsNotFound() throws Exception {
        long invalidId = 999L;
        Mockito.doThrow(new IllegalArgumentException("invalid id")).when(userRepository).deleteById(invalidId);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/{id}", invalidId)
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void getUserWithEmail_ValidEmail_ReturnsUser() throws Exception {
        User user = TestDataFactory.createUserWithEmail("user@example.com");
        UserDTO userDTO = TestDataFactory.createUserDTOWithEmail("user@example.com");
        Mockito.when(userRepository.findByEmail("user@example.com")).thenReturn(Optional.of(user));

        String expectedJson = ObjectConverter.convertObjectToJson(userDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/email/{email}",
                    "user@example.com")
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }

    @Test
    public void getUserWithEmail_InvalidEmail_ReturnsNotFound() throws Exception {
        Mockito.when(userRepository.findByEmail("nonexistent@example.com")).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/email/{email}",
                    "nonexistent@example.com")
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void getUserWithUsername_ValidUsername_ReturnsUser() throws Exception {
        User user = TestDataFactory.createUserWithUsername("username");
        UserDTO userDTO = TestDataFactory.createUserDTOWithUsername("username");
        Mockito.when(userRepository.findByUserName("username")).thenReturn(Optional.of(user));

        String expectedJson = ObjectConverter.convertObjectToJson(userDTO);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/username/{username}", "username")
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }

    @Test
    public void getUserWithUsername_InvalidUsername_ReturnsNotFound() throws Exception {
        Mockito.when(userRepository.findByUserName("nonexistentUsername")).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/username/{username}",
                    "nonexistentUsername")
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

 */

}
