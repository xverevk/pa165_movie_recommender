package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.facade.UserFacade;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.servers.Server;
import io.swagger.v3.oas.annotations.servers.ServerVariable;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@OpenAPIDefinition(
    info = @Info(title = "User Management And Authentication",
        version = "0.1.0",
        description = """
            Microservice for registering and logging in users as well as their authentication.
            The API has operations for:
            - creating a user account
            - updating user account information
            - deleting a user account
            - getting a specific user account by its username
            - getting all the registered users
            """,
        contact = @Contact(name = "Matej Vavro", email = "536408@mail.muni.cz"),
        license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.html")
    ),
    servers = @Server(description = "my server", url = "{scheme}://{server}:{port}", variables = {
        @ServerVariable(name = "scheme", allowableValues = {"http", "https"}, defaultValue = "http"),
        @ServerVariable(name = "server", defaultValue = "localhost"),
        @ServerVariable(name = "port", defaultValue = "8084"),
    })
)
@Tag(name = "User management and authentication",
    description = "Microservice for user management CRUD and authentication")
@RequestMapping(path = "/api/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserRestController {

    private final UserFacade userFacade;

    @Autowired
    public UserRestController(final UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @Operation(summary = "Returns all registered users paginated",
        description = "Returns a list of all the registered users paginated, filtered by user type if specified")
    @GetMapping(path = "")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Page<UserDTO>> getAllUsers(Pageable pageable,
                                                     @RequestParam(required = false) UserType userType) {
        Page<UserDTO> users;
        if (userType != null) {
            users = userFacade.getAllUsersWithUserType(userType, pageable);
        } else {
            users = userFacade.getAllUsers(pageable);
        }
        return ResponseEntity.ok(users);
    }

    @Operation(summary = "Creates a user",
        description = "Creates a user with the values that "
            + "are specified in the payload")
    @PostMapping(path = "")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody final UserRegistrationDTO user) {
        UserDTO createdUser = userFacade.createUser(user);
        return ResponseEntity.ok(createdUser);
    }

    @Operation(summary = "Returns the specified user",
        description = "Returns a user with the specified id ")
    @GetMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> getUser(@PathVariable final Long id) {
        UserDTO foundUser = userFacade.getUser(id);
        return ResponseEntity.ok(foundUser);
    }

    @Operation(summary = "Returns the specified user",
        description = "Returns a user with the specified email ")
    @GetMapping(path = "email/{email}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> getUserWithEmail(@PathVariable final String email) {
        UserDTO foundUser = userFacade.getUserWithEmail(email);
        return ResponseEntity.ok(foundUser);
    }

    @Operation(summary = "Returns the specified user",
        description = "Returns a user with the specified username ")
    @GetMapping(path = "username/{username}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<UserDTO> getUserWithUsername(@PathVariable final String username) {
        UserDTO foundUser = userFacade.getUserWithUsername(username);
        return ResponseEntity.ok(foundUser);
    }

    @Operation(summary = "Updates information about a user",
        description = "Updates information about a user by changing his attributes "
            + "with the ones specified in the payload")
    @PutMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Void> updateUser(@PathVariable final Long id,
                                           @Valid @RequestBody final UserUpdateDTO userDetails) {
        userFacade.updateUser(id, userDetails);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Deletes a user", description = "Deletes a user with the specified id")
    @DeleteMapping(path = "/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<Void> deleteUser(@PathVariable final long id) {
        userFacade.deleteUser(id);
        return ResponseEntity.noContent().build();
    }
}
