package cz.muni.fi.pa165.moviesmanagement.repository;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>, JpaSpecificationExecutor<Movie> {
    /**
     * Retrieves a movie by its name.
     *
     * @param name The name of the movie.
     * @return A list of movies with the given name.
     */
    @Query("SELECT m FROM Movie m WHERE m.name LIKE %:name%")
    List<Movie> findByName(@Param("name") String name);

    /**
     * Retrieves a movie by its genre.
     *
     * @param genre The genre of the movie.
     * @return A list of movies with the given genre.
     */
    @Query("SELECT m FROM Movie m WHERE :genre member of m.genres")
    List<Movie> findByGenre(@Param("genre") MovieGenre genre);
}
