package cz.muni.fi.pa165.moviesmanagement.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.facade.MovieFacade;
import cz.muni.fi.pa165.moviesmanagement.utils.KeycloakTokenProvider;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.HashSet;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith({SpringExtension.class, MockitoExtension.class})
@WebMvcTest(MovieController.class)
@AutoConfigureMockMvc
public class MovieControllerWebMvcIT {

    @Autowired
    private MockMvc mockMvc;

    private final KeycloakTokenProvider keycloakTokenProvider = new KeycloakTokenProvider();
    private String accessToken;

    @MockBean
    private MovieFacade movieFacade;

    @Autowired
    private ObjectMapper objectMapper;
    @Value("${info.app.oauth2.client-id}")
    private String clientId;

    @Value("${info.app.oauth2.client-secret}")
    private String clientSecret;

    @Value("${info.app.oauth2.username}")
    private String user;

    @Value("${info.app.oauth2.password}")
    private String password;

    @Value("${info.app.oauth2.realm}")
    private String realm;

    @Value("${info.app.oauth2.url}")
    private String url;

    @BeforeEach
    public void setup() {
        keycloakTokenProvider.setClientId(clientId);
        keycloakTokenProvider.setClientSecret(clientSecret);
        keycloakTokenProvider.setUser(user);
        keycloakTokenProvider.setPassword(password);
        keycloakTokenProvider.setRealm(realm);
        keycloakTokenProvider.setUrl(url);
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }
    @Test
    public void find_noParameters_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.find(null, null, null)).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/find").header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

    @Test
    public void find_byValidId_returnsMovieDto() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.find(1L, null, null)).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/find?id=1").header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

    @Test
    public void find_byValidName_returnsMovieDto() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("TestMovie");
        when(movieFacade.find(null, "TestMovie", null)).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/find?name=TestMovie").header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("TestMovie"));
    }

    @Test
    public void find_byValidGenre_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        when(movieFacade.find(null, null, MovieGenre.COMEDY)).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/find?genre=COMEDY").header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

    @Test
    public void addMovie_allCorrect_returnsMovieDto() throws Exception {
        CreateMovieDto createMovieDto = new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "Test Description",
                new HashSet<>(Collections.singletonList(MovieGenre.ACTION)),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );

        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        movie.setGenres(Collections.singleton(MovieGenre.COMEDY));
        when(movieFacade.addMovie(createMovieDto)).thenReturn(movie);

        mockMvc.perform(post("/api/movies").header("Authorization", "Bearer " + accessToken)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(createMovieDto)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").value("Test Movie"));
    }

    @Test
    public void deleteMovie_allCorrect_returnsTrue() throws Exception {
        when(movieFacade.deleteMovie(1L)).thenReturn(true);

        mockMvc.perform(delete("/api/movies/1").header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(true));
    }

    @Test
    public void getUsersFavoriteMovies_allCorrect_returnsMovieDtoCollection() throws Exception {
        MovieDto movie = new MovieDto();
        movie.setId(1L);
        movie.setName("Test Movie");
        Long userId = 1L;
        when(movieFacade.getUsersFavoriteMovies(1L)).thenReturn(Collections.singletonList(movie));

        mockMvc.perform(get("/api/movies/user/{userId}/favorite", userId).header("Authorization", "Bearer " + accessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").value("Test Movie"));
    }

}
