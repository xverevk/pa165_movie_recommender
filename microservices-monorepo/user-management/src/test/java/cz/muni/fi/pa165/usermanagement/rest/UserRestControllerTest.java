package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.facade.UserFacade;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
class UserRestControllerTest {
    // Cannot test negative cases because throwing exceptions will not get caught, because in unit tests the
    // RestGlobalExceptionHandling is not active and will not catch them
    // will need to be done in integration tests

    /**
     * Successful response code.
     */
    public static final int RESPONSE_OK = 200;

    /**
     * Not found response code.
     */
    public static final int RESPONSE_NO_CONTENT = 204;

    @Mock
    private UserFacade userFacade;

    @InjectMocks
    private UserRestController userRestController;

    @Test
    void getAllUsers_Valid_200StatusCodeAnd10DTOClasses() {
        Pageable pageable = PageRequest.of(0, 10);
        List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(10L);
        Page<UserDTO> page = new PageImpl<>(userDTOs);

        Mockito.when(userFacade.getAllUsers(pageable)).thenReturn(page);

        ResponseEntity<Page<UserDTO>> result = userRestController.getAllUsers(pageable, null);

        assertSuccessfulCallMultiple(result, page);
        Mockito.verify(userFacade, Mockito.times(1)).getAllUsers(pageable);
    }

    @Test
    void getAllUsers_ValidWithUserType_200StatusCodeAnd10DTOClasses() {
        Pageable pageable = PageRequest.of(0, 10);
        List<UserDTO> userDTOs = TestDataFactory.createUserDTOMultiple(10L);
        Page<UserDTO> page = new PageImpl<>(userDTOs);

        Mockito.when(userFacade.getAllUsersWithUserType(UserType.USER, pageable)).thenReturn(page);

        ResponseEntity<Page<UserDTO>> result = userRestController.getAllUsers(pageable, UserType.USER);

        assertSuccessfulCallMultiple(result, page);
        Mockito.verify(userFacade, Mockito.times(1))
            .getAllUsersWithUserType(UserType.USER, pageable);
    }

    @ParameterizedTest
    @EnumSource(UserType.class)
    void createUser_ValidUser_StatusCode200AndValidUser(final UserType userType) {
        UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO("newUser", userType);
        UserDTO userDTO = TestDataFactory.createUserDTO(1L, userType);

        Mockito.when(userFacade.createUser(userRegistrationDTO)).thenReturn(userDTO);
        ResponseEntity<UserDTO> result = userRestController.createUser(userRegistrationDTO);

        assertSuccessfulCall(result, userDTO);
        Mockito.verify(userFacade, Mockito.times(1)).createUser(userRegistrationDTO);
    }

    @Test
    void getUser_Valid_200StatusCodeAndValidUser() {
        UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);

        Mockito.when(userFacade.getUser(1L)).thenReturn(userDTO);

        ResponseEntity<UserDTO> result = userRestController.getUser(1L);

        assertSuccessfulCall(result, userDTO);
        Mockito.verify(userFacade, Mockito.times(1)).getUser(1L);
    }

    @Test
    void getUserWithEmail_Valid_200StatusCodeAndValidUser() {
        String testEmail = "abc@gmail.com";
        UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);

        Mockito.when(userFacade.getUserWithEmail(testEmail)).thenReturn(userDTO);

        ResponseEntity<UserDTO> result = userRestController.getUserWithEmail(testEmail);

        assertSuccessfulCall(result, userDTO);
        Mockito.verify(userFacade, Mockito.times(1)).getUserWithEmail(testEmail);
    }

    @Test
    void getUserWithUsername_Valid_200StatusCodeAndValidUser() {
        String testUsername = "anonym";
        UserDTO userDTO = TestDataFactory.createUserDTO(1L, UserType.USER);

        Mockito.when(userFacade.getUserWithEmail(testUsername)).thenReturn(userDTO);

        ResponseEntity<UserDTO> result = userRestController.getUserWithEmail(testUsername);

        assertSuccessfulCall(result, userDTO);
        Mockito.verify(userFacade, Mockito.times(1)).getUserWithEmail(testUsername);
    }

    @Test
    void updateUser_Valid_SucceedsNoContent() {
        UserUpdateDTO userUpdateDTO = TestDataFactory.createUserUpdateDTO("updatedUser", UserType.ADMIN);

        ResponseEntity<Void> result = userRestController.updateUser(1L, userUpdateDTO);

        assertResultNoContent(result);
        Mockito.verify(userFacade, Mockito.times(1)).updateUser(1L, userUpdateDTO);
    }

    @Test
    void deleteUser_Valid_SucceedsNoContent() {
        ResponseEntity<Void> result = userRestController.deleteUser(1L);

        assertResultNoContent(result);
        Mockito.verify(userFacade, Mockito.times(1)).deleteUser(1L);
    }

    private void assertSuccessfulCallMultiple(ResponseEntity<Page<UserDTO>> result, Page<UserDTO> page) {
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(RESPONSE_OK));
        Page<UserDTO> resultPage = result.getBody();
        Assertions.assertEquals(page, resultPage);
    }

    private void assertSuccessfulCall(ResponseEntity<UserDTO> result, UserDTO userDTO) {
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(RESPONSE_OK));
        UserDTO resultUser = result.getBody();
        assertResultStatus(userDTO, resultUser);
    }

    private void assertResultStatusMultiple(Page<UserDTO> page, Page<UserDTO> expectedPage) {
//		Assertions.assertEquals(page, expectedPage);
//		assertThat(returnedUsers).isNotEmpty();
//		assertThat(returnedUsers.size()).isEqualTo(expectedUsers.size());
//		assertThat(returnedUsers).containsAll(expectedUsers);
    }

    private void assertResultStatus(UserDTO returnedUsers, UserDTO expectedUsers) {
        assertThat(returnedUsers).isNotNull();
        assertThat(returnedUsers).isEqualTo(expectedUsers);
    }

    private void assertResultNoContent(ResponseEntity<Void> result) {
        assertThat(result).isNotNull();
        assertThat(result.getStatusCode()).isEqualTo(HttpStatusCode.valueOf(RESPONSE_NO_CONTENT));
        assertThat(result.getBody()).isNull();
    }
}
