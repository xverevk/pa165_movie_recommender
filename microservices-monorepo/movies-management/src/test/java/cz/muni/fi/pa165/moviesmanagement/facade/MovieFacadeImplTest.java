package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.mapper.MovieMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.service.MovieService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


public class MovieFacadeImplTest {

    @Mock
    private MovieService movieService;

    @Mock
    private MovieMapper movieMapper;

    @InjectMocks
    private MovieFacadeImpl movieFacade;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void find_noParameters_returnsListOfMovies() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        when(movieService.find(null, null, null)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> result = movieFacade.find(null, null, null);

        // Assert
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.contains(movieDto));
    }

    @Test
    public void find_byValidId_returnsMovieDto() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        Long id = 1L;
        when(movieService.find(id, null, null)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> result = movieFacade.find(id, null, null);

        // Assert
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
    }

    @Test
    public void find_byInvalidId_returnsEmptyList() {
        // Arrange
        Long id = 1L;
        when(movieService.find(id, null, null)).thenReturn(Collections.emptyList());

        // Act
        Collection<MovieDto> result = movieFacade.find(id, null, null);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    public void find_byValidGenre_returnsListOfMovieDtos() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        MovieGenre genre = MovieGenre.ACTION;
        Set<MovieGenre> genres = Set.of(genre);
        when(movieService.find(null, null, genres)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> results = movieFacade.find(null, null, genre);

        // Assert
        assertFalse(results.isEmpty());
        assertEquals(1, results.size());
        assertTrue(results.contains(movieDto));
    }

    @Test
    public void find_byValidName_returnsMovieDto() {
        // Arrange
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        String name = "Test Movie";
        when(movieService.find(null, name, null)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> results = movieFacade.find(null, name, null);

        // Assert
        assertNotNull(results);
        assertFalse(results.isEmpty());
        assertEquals(movieDto, results.iterator().next());
    }

    @Test
    public void find_byInvalidName_returnsEmptyList() {
        // Arrange
        String name = "Test Movie";
        when(movieService.find(null, name, null)).thenReturn(Collections.emptyList());

        // Act
        Collection<MovieDto> result = movieFacade.find(null, name, null);

        // Assert
        assertTrue(result.isEmpty());
    }


    @Test
    public void addMovie_allCorrect_returnsMovieDto() {
        // Arrange
        CreateMovieDto createMovieDto = getSampleCreateMovieDto();
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        when(movieMapper.convertToEntity(createMovieDto)).thenReturn(movie);
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        MovieDto result = movieFacade.addMovie(createMovieDto);

        // Assert
        assertNotNull(result);
        assertEquals(movieDto, result);
    }

    // Test pro updateMovie metodu
    @Test
    public void updateMovie_allCorrect_returnsMovieDto() {
        // Arrange
        MovieDto movieDto = new MovieDto();
        Movie movie = new Movie();
        when(movieMapper.convertToEntity(movieDto)).thenReturn(movie);
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        MovieDto result = movieFacade.updateMovie(movieDto);

        // Assert
        assertNotNull(result);
        assertEquals(movieDto, result);
    }

    // Test pro deleteMovie metodu
    @Test
    public void deleteMovie_allCorrect_returnsTrue() {
        // Arrange
        Long id = 1L;
        Movie movie = new Movie();
        when(movieService.find(id, null, null)).thenReturn(Collections.singletonList(movie));

        // Act
        Boolean result = movieFacade.deleteMovie(id);

        // Assert
        assertTrue(result);
        verify(movieService, times(1)).delete(movie);
    }

    // Test pro deleteMovie metodu, když není nalezen žádný film
    @Test
    public void deleteMovie_notFoundId_returnsFalse() {
        // Arrange
        Long id = 1L;
        when(movieService.find(id, null, null)).thenReturn(Collections.emptyList());

        // Act
        Boolean result = movieFacade.deleteMovie(id);

        // Assert
        assertFalse(result);
        verify(movieService, never()).delete(any());
    }

    @Test
    public void getUsersFavoriteMovies_allCorrect_returnsListOfMovies() {
        // Arrange
        Long userId = 1L;
        Movie movie = new Movie();
        MovieDto movieDto = new MovieDto();
        when(movieService.getUsersFavoriteMovies(userId)).thenReturn(Collections.singletonList(movie));
        when(movieMapper.convertToDto(movie)).thenReturn(movieDto);

        // Act
        Collection<MovieDto> result = movieFacade.getUsersFavoriteMovies(userId);

        // Assert
        assertFalse(result.isEmpty());
        assertEquals(1, result.size());
        assertTrue(result.contains(movieDto));
    }

    @Test
    public void getUsersFavoriteMovies_noMovies_returnsEmptyList() {
        // Arrange
        Long userId = 1L;
        when(movieService.getUsersFavoriteMovies(userId)).thenReturn(Collections.emptyList());

        // Act
        Collection<MovieDto> result = movieFacade.getUsersFavoriteMovies(userId);

        // Assert
        assertTrue(result.isEmpty());
    }


    public CreateMovieDto getSampleCreateMovieDto() {
        return new CreateMovieDto(
                "Test Movie",
                "Test Director",
                "description",
                new HashSet<>(Collections.singletonList(MovieGenre.ACTION)),
                "testImageUrl",
                Collections.singletonList("Test Actor")
        );
    }
}