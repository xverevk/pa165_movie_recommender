package cz.muni.fi.pa165.moviesmanagement.dto;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

/**
 * Data Transfer Object for Movie.
 * This class encapsulates all information about a movie necessary for business logic and presentation layers.
 */
@Setter
@Getter
public class MovieDto {

    /**
     * -- GETTER --
     *  Gets the ID of the movie.
     * -- SETTER --
     *  Sets the ID of the movie.
     */
    private Long id;
    /**
     * -- GETTER --
     *  Gets the name of the movie.
     * -- SETTER --
     *  Sets the name of the movie.
     */
    @NotBlank(message = "Name cannot be empty")
    private String name;
    /**
     * -- GETTER --
     *  Gets the director of the movie.
     * -- SETTER --
     *  Sets the director of the movie.
     */
    @NotBlank(message = "Director cannot be empty")
    private String director;
    /**
     * -- GETTER --
     *  Gets the description of the movie.
     * -- SETTER --
     *  Sets the description of the movie.
     */
    @NotBlank(message = "Description cannot be empty")
    private String description;
    /**
     * -- GETTER --
     *  Gets the actors of the movie.
     * -- SETTER --
     *  Sets the actors of the movie.
     */
    @NotEmpty(message = "At least one actor must be provided")
    private List<String> actors;
    /**
     * -- GETTER --
     *  Gets the genres of the movie.
     * -- SETTER --
     *  Sets the genres of the movie.
     */
    @NotEmpty(message = "Genres cannot be empty")
    private Set<MovieGenre> genres;
    /**
     * -- GETTER --
     *  Gets the image URL of the movie.
     * -- SETTER --
     *  Sets the image URL of the movie.
     */
    private String imageUrl;

}
