package cz.muni.fi.pa165.usermanagement.rest;

import cz.muni.fi.pa165.usermanagement.api.UserDTO;
import cz.muni.fi.pa165.usermanagement.api.UserRegistrationDTO;
import cz.muni.fi.pa165.usermanagement.api.UserUpdateDTO;
import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.facade.UserFacade;
import cz.muni.fi.pa165.usermanagement.testutilities.JsonVerifier;
import cz.muni.fi.pa165.usermanagement.testutilities.KeycloakTokenProvider;
import cz.muni.fi.pa165.usermanagement.testutilities.ObjectConverter;
import cz.muni.fi.pa165.usermanagement.testutilities.TestDataFactory;
import jakarta.persistence.EntityNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.nio.charset.StandardCharsets;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = {cz.muni.fi.pa165.usermanagement.rest.UserRestController.class})
public class UserRestControllerWebMvcIT {

    @Value("${info.app.oauth2.client-id}")
    private String clientId;

    @Value("${info.app.oauth2.client-secret}")
    private String clientSecret;

    @Value("${info.app.oauth2.username}")
    private String user;

    @Value("${info.app.oauth2.password}")
    private String password;

    @Value("${info.app.oauth2.realm}")
    private String realm;

    @Value("${info.app.oauth2.url}")
    private String url;

    @Autowired
    private MockMvc mockMvc;

    private String accessToken;

    private final KeycloakTokenProvider keycloakTokenProvider = new KeycloakTokenProvider();

    @MockBean
    private UserFacade userFacade;

    @BeforeEach
    public void setup() {
        keycloakTokenProvider.setClientId(clientId);
        keycloakTokenProvider.setClientSecret(clientSecret);
        keycloakTokenProvider.setUser(user);
        keycloakTokenProvider.setPassword(password);
        keycloakTokenProvider.setRealm(realm);
        keycloakTokenProvider.setUrl(url);
        accessToken = keycloakTokenProvider.getKeycloakAccessToken();
    }

    @Test
    public void getAllUsers_NoParams_ReturnsAllUsers() throws Exception {
        Pageable pageable = PageRequest.of(0, 10);
        List<UserDTO> userDTOS = TestDataFactory.createUserDTOMultiple(10);
        Page<UserDTO> pageDTO = new PageImpl<>(userDTOS);

        Mockito.when(userFacade.getAllUsers(pageable)).thenReturn(pageDTO);

        String jsonResponse = mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .param("page", "0")
                .param("size", "10")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        JsonVerifier.verifyJsonResponse(jsonResponse, UserType.USER);
    }

    @Test
    public void getAllUsers_WithUserType_ReturnsFilteredUsers() throws Exception {
        Pageable pageable = PageRequest.of(0, 10);
        List<UserDTO> userDTOS = TestDataFactory.createUserDTOMultiple(10, UserType.ADMIN);
        Page<UserDTO> page = new PageImpl<>(userDTOS);

        Mockito.when(userFacade.getAllUsersWithUserType(UserType.ADMIN, pageable)).thenReturn(page);

        String jsonResponse = mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .param("userType", "ADMIN")
                .param("page", "0")
                .param("size", "10")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andReturn().getResponse().getContentAsString(StandardCharsets.UTF_8);
        JsonVerifier.verifyJsonResponse(jsonResponse, UserType.ADMIN);
    }

    @Test
    void getAllUsers_Invalid_404StatusCode() throws Exception {
        Pageable pageable = PageRequest.of(0, 10);
        Mockito.when(userFacade.getAllUsers(pageable)).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .param("page", "0")
                .param("size", "10")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).getAllUsers(pageable);
    }

    @Test
    void createUser_ValidUser_StatusCode200AndValidUser() throws Exception {
        UserRegistrationDTO userRegistrationDTO = TestDataFactory.createUserRegistrationDTO("testIdentifier", UserType.USER);
        UserDTO userDto = TestDataFactory.createUserDTO(1L, UserType.USER);
        String contentJson = ObjectConverter.convertObjectToJson(userRegistrationDTO);

        Mockito.when(userFacade.createUser(userRegistrationDTO)).thenReturn(userDto);

        mockMvc.perform(MockMvcRequestBuilders.post("/api/users")
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(contentJson)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    void getUser_ValidId_ReturnsUser() throws Exception {
        UserDTO userDto = TestDataFactory.createUserDTO(1L, UserType.USER);
        Mockito.when(userFacade.getUser(1L)).thenReturn(userDto);

        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/users/{id}", 1)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        UserDTO resultUser = ObjectConverter.convertJsonToObject(responseJson, UserDTO.class);
        assertEqualityOfDTO(resultUser, userDto);
        Mockito.verify(userFacade, Mockito.times(1)).getUser(1L);
    }

    @Test
    void getUser_InvalidId_ReturnsNotFound() throws Exception {
        Mockito.when(userFacade.getUser(999L)).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/{id}", 999)
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).getUser(999L);
    }

    @Test
    void getUserWithEmail_ValidEmail_ReturnsUser() throws Exception {
        UserDTO userDto = TestDataFactory.createUserDTOWithEmail("user@example.com");
        Mockito.when(userFacade.getUserWithEmail("user@example.com")).thenReturn(userDto);

        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/users/email/{email}", "user@example.com")
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        UserDTO resultUser = ObjectConverter.convertJsonToObject(responseJson, UserDTO.class);
        assertEqualityOfDTO(resultUser, userDto);
        Mockito.verify(userFacade, Mockito.times(1)).getUserWithEmail("user@example.com");
    }

    @Test
    void getUserWithEmail_InvalidEmail_ReturnsNotFound() throws Exception {
        Mockito.when(userFacade.getUserWithEmail("invalid@example.com")).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/email/{email}", "invalid@example.com")
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).getUserWithEmail("invalid@example.com");
    }

    @Test
    void getUserWithUsername_ValidUsername_ReturnsUser() throws Exception {
        UserDTO userDto = TestDataFactory.createUserDTOWithUsername("validUsername");
        Mockito.when(userFacade.getUserWithUsername("validUsername")).thenReturn(userDto);

        String responseJson = mockMvc.perform(MockMvcRequestBuilders.get("/api/users/username/{username}", "validUsername")
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);

        UserDTO resultUser = ObjectConverter.convertJsonToObject(responseJson, UserDTO.class);
        assertEqualityOfDTO(resultUser, userDto);
        Mockito.verify(userFacade, Mockito.times(1)).getUserWithUsername("validUsername");
    }

    @Test
    void getUserWithUsername_InvalidUsername_ReturnsNotFound() throws Exception {
        Mockito.when(userFacade.getUserWithUsername("invalidUsername")).thenThrow(EntityNotFoundException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/users/username/{username}", "invalidUsername")
                .header("Authorization", "Bearer " + accessToken)
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).getUserWithUsername("invalidUsername");
    }

    @Test
    void updateUser_ValidIdAndDetails_NoContentReturned() throws Exception {
        UserUpdateDTO userDetails = TestDataFactory.createUserUpdateDTO("test", UserType.USER);
        Mockito.doNothing().when(userFacade).updateUser(1L, userDetails);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", 1)
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectConverter.convertObjectToJson(userDetails)))
            .andExpect(status().isNoContent());

        Mockito.verify(userFacade, Mockito.times(1)).updateUser(1L, userDetails);
    }

    @Test
    void updateUser_InvalidId_NotFoundReturned() throws Exception {
        UserUpdateDTO userDetails = TestDataFactory.createUserUpdateDTO("test", UserType.USER);
        Mockito.doThrow(EntityNotFoundException.class).when(userFacade).updateUser(999L, userDetails);

        mockMvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", 999)
                .header("Authorization", "Bearer " + accessToken)
                .contentType(MediaType.APPLICATION_JSON)
                .content(ObjectConverter.convertObjectToJson(userDetails)))
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).updateUser(999L, userDetails);
    }

    @Test
    void deleteUser_ValidId_NoContentReturned() throws Exception {
        Mockito.doNothing().when(userFacade).deleteUser(1L);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/{id}", 1)
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(status().isNoContent());

        Mockito.verify(userFacade, Mockito.times(1)).deleteUser(1L);
    }

    @Test
    void deleteUser_InvalidId_NotFoundReturned() throws Exception {
        Mockito.doThrow(EntityNotFoundException.class).when(userFacade).deleteUser(999L);

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/users/{id}", 999)
                .header("Authorization", "Bearer " + accessToken)
            )
            .andExpect(status().isNotFound());

        Mockito.verify(userFacade, Mockito.times(1)).deleteUser(999L);
    }

    private void assertEqualityOfDTO(UserDTO result, UserDTO userDTOs) {
        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(userDTOs);
    }
}
