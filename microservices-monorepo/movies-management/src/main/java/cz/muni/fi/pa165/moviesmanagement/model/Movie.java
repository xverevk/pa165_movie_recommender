package cz.muni.fi.pa165.moviesmanagement.model;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "movie")
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, name = "name")
    private String name;

    @Column(nullable = false, name = "director")
    private String director;

    @Column(nullable = false, name = "description")
    private String description;

    @ElementCollection
    @Column(name = "actors")
    private List<String> actors;

    /**
     * The genres of the movie.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "genre")
    @ElementCollection(targetClass = MovieGenre.class)
    private Set<MovieGenre> genres;
    private String imageUrl;

}
