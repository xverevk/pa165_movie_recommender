package cz.muni.fi.pa165.ratingsandreviews.utils;

import lombok.NoArgsConstructor;
import lombok.Setter;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Setter
@Component
@NoArgsConstructor
public class KeycloakTokenProvider {

    @Value("${info.app.oauth2.client-id}")
    private String clientId;

    @Value("${info.app.oauth2.client-secret}")
    private String clientSecret;

    @Value("${info.app.oauth2.username}")
    private String user;

    @Value("${info.app.oauth2.password}")
    private String password;

    @Value("${info.app.oauth2.realm}")
    private String realm;

    @Value("${info.app.oauth2.url}")
    private String url;

    public String getKeycloakAccessToken() {
        try (Keycloak keycloak = KeycloakBuilder.builder()
                .serverUrl(url)
                .realm(realm)
                .username(user)
                .password(password)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .grantType("password")
                .build()) {
            return keycloak.tokenManager().getAccessToken().getToken();
        }
    }
}