package cz.muni.fi.pa165.usermanagement.service;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import cz.muni.fi.pa165.usermanagement.data.model.User;
import cz.muni.fi.pa165.usermanagement.data.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Primary
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public Page<User> getAllUsers(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<User> getAllUsersWithUserType(final UserType userType, Pageable pageable) {
        return userRepository.findAllByUserTypeIs(userType, pageable);
    }

    @Override
    @Transactional
    public User createUser(final User user) {
        return userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User getUser(final long id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new EntityNotFoundException("User with id " + id + " does not exist"));
    }


    @Override
    @Transactional(readOnly = true)
    public User getUserWithEmail(String email) {
        return userRepository.findByEmail(email)
            .orElseThrow(() -> new EntityNotFoundException("User with email " + email + " does not exist"));
    }

    @Override
    @Transactional(readOnly = true)
    public User getUserWithUsername(String username) {
        return userRepository.findByUserName(username)
            .orElseThrow(() -> new EntityNotFoundException("User with username " + username + " does not exist"));
    }

    @Override
    @Transactional
    public void updateUser(final User updatedUser) {
        try {
            userRepository.save(updatedUser);
        } catch (IllegalArgumentException ex) {
            throw new EntityNotFoundException(ex);
        }
    }

    @Override
    @Transactional
    public void deleteUser(final long id) {
        try {
            userRepository.deleteById(id);
        } catch (IllegalArgumentException ex) {
            throw new EntityNotFoundException(ex);
        }
    }
}
