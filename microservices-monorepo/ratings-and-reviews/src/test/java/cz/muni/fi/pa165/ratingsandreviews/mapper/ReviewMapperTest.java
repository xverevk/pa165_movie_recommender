package cz.muni.fi.pa165.ratingsandreviews.mapper;

import cz.muni.fi.pa165.ratingsandreviews.dto.ReviewDTO;
import cz.muni.fi.pa165.ratingsandreviews.model.Review;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ReviewMapperTest {

    ReviewMapper reviewMapper = new ReviewMapper();

    @Test
    void dtoToEntity_validReviewDTO_returnsReview() {
        // Arrange
        ReviewDTO reviewDTO = new ReviewDTO(1L, 2L, 3L, null, 4.5d);

        // Act
        Review review = reviewMapper.dtoToEntity(reviewDTO);

        // Assert
        Assertions.assertNotNull(review);
        Assertions.assertEquals(reviewDTO.getId(), review.getId());
        Assertions.assertEquals(reviewDTO.getMovieId(), review.getMovieId());
        Assertions.assertEquals(reviewDTO.getUserId(), review.getUserId());
        Assertions.assertEquals(reviewDTO.getOverallRating(), review.getOverallRating());
    }

    @Test
    void entityToDto_validReview_returnsReviewDTO() {
        // Arrange
        Review review = new Review();
        review.setId(1L);
        review.setMovieId(2L);
        review.setUserId(3L);
        review.setOverallRating(4.5);

        // Act
        ReviewDTO reviewDTO = reviewMapper.entityToDto(review);

        // Assert
        Assertions.assertNotNull(reviewDTO);
        Assertions.assertEquals(review.getId(), reviewDTO.getId());
        Assertions.assertEquals(review.getMovieId(), reviewDTO.getMovieId());
        Assertions.assertEquals(review.getUserId(), reviewDTO.getUserId());
        Assertions.assertEquals(review.getOverallRating(), reviewDTO.getOverallRating());
    }
}
