package cz.muni.fi.pa165.moviesmanagement.repository;

import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.utils.MovieMockDataFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
public class MovieRepositoryTest {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private TestEntityManager entityManager;

    @BeforeEach
    void initData() {
        List<Movie> movies = MovieMockDataFactory.createAllMockMoviesWithoutId();
        movies.forEach(entityManager::persist);
    }

    @Test
    public void findByName_validName_returnMovie() {
        // Arrange
        Movie movie = MovieMockDataFactory.createShawshankWithoutId();
        entityManager.persist(movie);
        entityManager.flush();

        // Act
        List<Movie> found = movieRepository.findByName(movie.getName());

        // Assert
        assertThat(found.getFirst().getName()).isEqualTo(movie.getName());
    }

    @Test
    public void findByName_invalidName_returnsNothing() {
        // Arrange
        Movie movie = MovieMockDataFactory.createShawshankWithoutId();
        entityManager.persist(movie);
        entityManager.flush();

        // Act
        List<Movie> found = movieRepository.findByName("Invalid Name");

        // Assert
        assertTrue(found.isEmpty());
    }



    @Test
    public void findByGenre_validGenre_returnMovies() {
        // Arrange
        Movie movie = MovieMockDataFactory.createShawshankWithoutId();
        entityManager.persist(movie);
        entityManager.flush();

        // Act
        List<Movie> found = movieRepository.findByGenre(MovieGenre.DRAMA);

        // Assert
        assertThat(found.getFirst().getGenres()).contains(MovieGenre.DRAMA);
    }

    @Test
    public void findByGenre_invalidGenre_returnEmptyList() {
        // Arrange
        Movie movie = MovieMockDataFactory.createShawshankWithoutId();
        entityManager.persist(movie);
        entityManager.flush();

        // Act
        List<Movie> found = movieRepository.findByGenre(MovieGenre.ACTION);

        // Assert
        assertTrue(found.isEmpty());
    }
}

