package cz.muni.fi.pa165.usermanagement.data.model;

import cz.muni.fi.pa165.usermanagement.data.enums.UserType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "user_table")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    public static final int USERNAME_MAX_LENGTH = 100;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private long id;

    @Column(name = "username", unique = true, length = USERNAME_MAX_LENGTH)
    private String userName;

    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "encrypted_password")
    private String encryptedPassword;

    @Column(name = "user_type")
    @Enumerated(EnumType.STRING)
    private UserType userType;
}
