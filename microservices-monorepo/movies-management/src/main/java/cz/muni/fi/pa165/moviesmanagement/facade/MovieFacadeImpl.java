package cz.muni.fi.pa165.moviesmanagement.facade;

import cz.muni.fi.pa165.moviesmanagement.dto.CreateMovieDto;
import cz.muni.fi.pa165.moviesmanagement.dto.MovieDto;
import cz.muni.fi.pa165.moviesmanagement.enums.MovieGenre;
import cz.muni.fi.pa165.moviesmanagement.mapper.MovieMapper;
import cz.muni.fi.pa165.moviesmanagement.model.Movie;
import cz.muni.fi.pa165.moviesmanagement.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class MovieFacadeImpl implements MovieFacade {

    private final MovieService movieService;
    private final MovieMapper movieMapper;

    /**
     * Constructor for MovieFacadeImpl.
     *
     * @param movieService The movie service.
     * @param movieMapper  The movie mapper.
     */
    @Autowired
    public MovieFacadeImpl(final MovieService movieService, final MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }


    @Override
    public Collection<MovieDto> find(Long id, String name, MovieGenre genre) {
        Set<MovieGenre> genres = genre == null ? null : Set.of(genre);
        Collection<Movie> movies = movieService.find(id, name, genres);
        return movies.stream().map(movieMapper::convertToDto).collect(Collectors.toList());
    }

    /**
     * Adds a new movie to the repository.
     *
     * @param movieDto The movie DTO to add.
     * @return The added movie DTO.
     */
    @Override
    @Transactional
    public MovieDto addMovie(final CreateMovieDto movieDto) {
        Movie movie = movieMapper.convertToEntity(movieDto);
        movieService.add(movie);
        return movieMapper.convertToDto(movie);
    }

    /**
     * Updates a movie in the repository.
     *
     * @param movieDto The movie DTO to update.
     * @return The updated movie DTO.
     */
    @Override
    @Transactional
    public MovieDto updateMovie(final MovieDto movieDto) {
        Movie movie = movieMapper.convertToEntity(movieDto);
        movieService.update(movie);
        return movieMapper.convertToDto(movie);
    }

    /**
     * Deletes a movie by its identifier.
     *
     * @param id The identifier of the movie to delete.
     * @return true if the movie was successfully deleted, false otherwise.
     */
    @Override
    @Transactional
    public Boolean deleteMovie(final Long id) {
        Optional<Movie> movie = movieService.find(id, null, null).stream().findFirst();
        if (movie.isEmpty()) {
            return false;
        }
        movie.ifPresent(movieService::delete);
        return true;
    }

    /**
     * Retrieves all favorite movies of a user.
     *
     * @param userId The identifier of the user.
     * @return A collection of movie DTOs that are the user's favorite movies.
     */
    @Override
    @Transactional(readOnly = true)
    public List<MovieDto> getUsersFavoriteMovies(final Long userId) {
        return movieService.getUsersFavoriteMovies(userId).stream()
                .map(movieMapper::convertToDto)
                .toList();
    }
}
